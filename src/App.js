import React, {useEffect} from 'react';

import api from './services/api'

import Routes from './routes'
import GlobalStyle from './styles/global'

function App() {

  return (
  <>
    <Routes />
    <GlobalStyle />
  </>
  );
}

export default App;
