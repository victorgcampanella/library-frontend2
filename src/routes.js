import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import Login from './pages/Login'
import Register from './pages/Register'
import ListBooks from './pages/ListBook'
import AddBooks from './pages/AddBook'

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={Login}/>
        <Route path='/register' exact component={Register}/>
        <Route path='/users/books' exact component={ListBooks}/>
        <Route path='/admin/books' exact component={AddBooks}/>
      </Switch>
    </BrowserRouter>
  )
}