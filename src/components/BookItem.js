import React from 'react';

function BookHeader({ book }) {
  return (
    <div className="post-header">
      <div className="details">
        <span>{book.name}</span>
      </div>
    </div>
  );
}

function BookComments({ comments }) {
  return (
    <div className="post-comments">
      <div className="divider" />
      {comments.map(comment => (
        <div className="comment">
            <span>{comment}</span>
        </div>
      ))}
    </div>
  );
}

function BookItem({ book, comments }) {
  return (
    <div className="post">
      <BookHeader book={book} />
      <BookComments comments={comments} />
    </div>
  );
}

export default BookItem;