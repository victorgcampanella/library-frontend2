/* eslint-disable no-unused-expressions */
import React, { Component, useEffect } from 'react';

import { FaPlus } from 'react-icons/fa'

import api from '../../services/api'

import { Container, Form, SubmitButton, List } from './styles';

class ListBook extends Component {
  state = {
    newComment: '',
    books: []
  }

  handleSubmit = async e => {
    e.preventDefault()

    const { books } = this.state

    const response = await api.get(`/books`)

    this.setState({
      books: [...books, response],
    })

    console.log(books)
  }

  render() {
    const { newComment, books } = this.state

    this.handleSubmit()

    return (
      <Container>
        <List>
          {books.map(book => (
            <li key={book.name}>
              <span>{book.name}</span>

              <li>
                {book.comments.map( comment => (
                  <span>{comment}</span>
                ))}
              </li>

              {/* <Form >
                <input
                  type="text"
                  placeholder="Adicionar comentário"
                  value={newComment}
                  onChange={this.handleInputChange}
                />

                <SubmitButton>
                  <FaPlus color="#fff" size={14} />
                </SubmitButton>
              </Form> */}
            </li>
          ))}
        </List>
      </Container>
    );
  }
}

export default ListBook;