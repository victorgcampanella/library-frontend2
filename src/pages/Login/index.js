/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {Link} from 'react-router-dom'

import { Container, Form } from './styles';

function Login() {

  return (
    <Container>
      <Form>
        <input type="email" placeholder="Digite seu e-mail"/>
        <input type="password" placeholder="Digite sua senha"/>

        <button type= "submit">
          <Link to="/admin/books">Acessar</Link>
        </button>
        <Link to="/register">Faça seu cadastro</Link>
      </Form>
    </Container>
  );
}

export default Login;