import React from 'react';
import {Link} from 'react-router-dom'

import { Container, Form } from './styles';

function Register() {
  return (
    <Container>
      <Form>
        <input type="text" placeholder="Digite seu nome"/>
        <input type="email" placeholder="Digite seu e-mail"/>
        <input type="password" placeholder="Digite sua senha"/>

        <button type="submit">Acessar</button>
        <Link to="/">Já tenho cadastro</Link>
      </Form>
    </Container>
  );
}

export default Register;