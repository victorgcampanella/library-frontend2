import styled from 'styled-components'
import {darken} from 'polished'

export const Container = styled.div`
  display: flex;
  height: 100%;
  text-align: center;
  justify-content: center;
  align-items: center;
`;

export const Form = styled.form`
  width: 300px;
  display: flex;
  flex-direction: column;
  margin-top: 30px;

  input {
      background: rgba(0, 0, 0, 0.1);
      border: 0;
      border-radius: 6px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }
    }

    button {
      margin: 5px 0 0;
      height: 44px;
      background: #4e41ee;
      font-weight: bold;
      color: #fff;
      border: 0;
      border-radius: 10px;
      font-size: 16px;
      transition: background 0.2s;

      &:hover {
        background: ${darken(0.03, '#4e41ee')}
      }
    }

    a {
      color: #fff;
      margin-top: 15px;
      font-size: 16px;
      opacity: 0.8;

      &:hover {
        opacity: 1;
      }
    }
`;