import React, {Component} from 'react';

import {FaBookOpen, FaPlus, FaTrashAlt, FaPen} from 'react-icons/fa'

import api from '../../services/api'

import { Container, Form, SubmitButton, List } from './styles';

class AddBook extends Component {
  state = {
    newBook: '',
    books: []
  }

  handleInputChange = e => {
    this.setState({newBook: e.target.value})
  }

  handleSubmit = async e => {
    e.preventDefault()

    const {newBook, books} = this.state

    const response = await api.post(`/books`, {
      name: newBook
    })

    const data = {
      name: response.data.name,
    }

    this.setState({
      books: [...books, data],
      newBook: ''
    })
  }

  render() {
    const {newBook, books} = this.state

    return (
      <Container>
        <h1>
          <FaBookOpen />
          Livros
        </h1>
  
        <Form onSubmit={this.handleSubmit}>
          <input 
            type="text" 
            placeholder="Adicionar livro"
            value={newBook}
            onChange={this.handleInputChange}
          />
  
        <SubmitButton>
          <FaPlus color="#fff" size={14}/>
        </SubmitButton>
        </Form>

        <List>
          {books.map(book => (
            <li key={book.name}>
              <span>{book.name}</span>
              <FaTrashAlt color="red" size={14}/>
              <FaPen color="#6359de" size={14}/>
            </li>
          ))}
        </List>
      </Container>
    );
  }
}

export default AddBook;